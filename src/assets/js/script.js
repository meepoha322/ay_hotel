$(document).ready(function () {

    //Правила валидации
    var _NamePhone = {
        name: {
            required: true,
        },
        phone: {
            required: true,
            rus_phone: true,

        },
    };

    var _NamePhoneEmail = {
        name: {
            required: true,
        },
        email: {
            required: true,
            email: true,

        },
        phone: {
            required: true,
            rus_phone: true,
        },
    };

    var _MessagesForm = {
        email: {
            required: 'Введите Email',
            email: 'Введите валидный Email',
        },
        name: {
            required: 'Введите свое имя',
        },
        phone: {
            required: 'Введите номер телефона',
        },
        comment: {
            required: 'Введите комментарий',
        },
    };

    var optionsDate = {
        timezone: 'UTC',
        hour: 'numeric',
        minute: 'numeric',
        timeZoneName: 'short',
    };

    var TZ_TIME = new Date();


    /**HEADER*/
    var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    if (scrolled > 10) {
        $('.header').addClass('scrolled');
    } else {
        $('.header').removeClass('scrolled');
    }
    window.onscroll = function () {
        var scrolled = window.pageYOffset || document.documentElement.scrollTop;
        if (scrolled > 10) {
            $('.header').addClass('scrolled');
        } else {
            $('.header').removeClass('scrolled');
        }
    };
    /**HEADER*/

    $(document).ready(function () {
        $("a.ancor").click(function () {
            var e = $(this).attr("href"),
                t = $(e).offset().top;
            return $("html,body").animate({
                scrollTop: t
            }, 500), !1
        })
    });


    /**TABS*/
    $('.otdel_tabs a').click(function (e) {
        e.preventDefault();
        $(".otdel_tab-fullbtn").hide("slow");
        $('.otdel_tab').removeClass('active');
        $('.otdel_tabs-pane').removeClass('active');
        $($(this).attr('href')).addClass('active');
        $('.otdel_slider').slick('slickNext');
        $(".otdel_tab-fullbtn").show("slow");
    });


    /**SLIDER**/
    var $slider = $('.otdel_slider');

    $slider.slick({
        arrows: true,
        prevArrow: '<button type="button" class="slick-custom-prev"><i class="fa fa-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-custom-next"><i class="fa fa-angle-right"></i></button>',
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        dotsClass: 'otdel_tabs-slider-dots'
    });

    //Magnific

    $slider.each(function () {
        $(this).find('a').magnificPopup({
            type: 'image',
            gallery: {
                tCounter: '<span class="mfp-counter">%curr% из %total%</span>',
                enabled: true
            },
            mainClass: 'mfp-with-zoom', // this class is for CSS animation below
            preloader: false,
            fixedContentPos: false,
            removalDelay: 300,
        });
    });

    $('.btn-popup').magnificPopup({
        type: 'inline',
        mainClass: 'mfp-with-zoom', // this class is for CSS animation below
        removalDelay: 300,
        zoom: {
            enabled: true, // By default it's false, so don't forget to enable it
            duration: 300, // duration of the effect, in milliseconds
            easing: 'ease-in-out', // CSS transition easing function

        },
        callbacks: {
            open: function () {
                var elem = $($(this)[0].content);
                setTimeout(function () {
                    elem.find('input:eq(0)').focus();
                }, 500);
            }
        }
    });


    //Валидация форм
    /** Global **/

    $(".contact-form__phone").mask('+7(999)999-99-99');


    jQuery.validator.addMethod("rus_phone", function (value, element) {
        var matches = value.match(/(\d+)/g);
        return matches.join('').length === 11;
    }, "");
    /** Global **/


    function ajaxSend($_FORM) {
        var buttonText = $_FORM.find('[type="submit"]').text();
        var buttonColortext = $_FORM.find('[type="submit"]').css('color');
        var inputTzTime = $("<input type='hidden' name='tz_time'>").val(TZ_TIME.toLocaleString("ru", optionsDate));
        $_FORM.append(inputTzTime);


        $.ajax({
            method: 'POST',
            dataType: 'json',
            url: $_FORM.attr('action'),
            data: $_FORM.serialize(),
            beforeSend: function () {
                $_FORM.find('input,button').prop('disabled', true);

            },
            success: function (data) {
                if (data.success === true) {

                    //$.magnificPopup.close();
                    if (window.ga) ga('send', 'event', 'form', 'goal');
                    if (window.yaCounter47019384) yaCounter47019384.reachGoal('order');
                    $.magnificPopup.open({
                        items: {
                            src: $('#thanks-popup')
                        },
                        midClick: true,
                        type: 'inline',
                        mainClass: 'mfp-with-zoom', // this class is for CSS animation below
                        removalDelay: 300,
                        zoom: {
                            enabled: true, // By default it's false, so don't forget to enable it
                            duration: 300, // duration of the effect, in milliseconds
                            easing: 'ease-in-out', // CSS transition easing function

                        },
                        callbacks: {
                            open: function () {
                                var elem = $($(this)[0].content);
                                setTimeout(function () {
                                    elem.find('input:eq(0)').focus();
                                }, 500);
                            }
                        }
                    });
                    $_FORM.find('input').val("");
                    document.getElementById($_FORM.attr('id')).reset();
                }

            },
            complete: function (data) {
                //$_FORM.find('[type="submit"]').html(buttonText).css('color', buttonColortext);
                $_FORM.find('input,button').prop('disabled', false);
            }
        });
        event.preventDefault();
    }


    //Форма эксклюзивного предложения
    $('#ex_offer').validate({
        rules: _NamePhoneEmail,
        messages: _MessagesForm,
        errorElement: "div",
        errorPlacement: function (error, element) {
            /*error.addClass("form-control-feedback");
             if (element.prop("type") === "checkbox") {
             error.insertAfter(element.parent("label"));
             } else {
             error.insertAfter(element);
             }*/
        },
        highlight: function (element, errorClass, validClass) {
            $(element).closest('div').addClass('shake');
            setTimeout(function () {
                $(element).closest('div').removeClass('shake');
            }, 1500);
            $(element).closest('div').addClass('has-danger');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest('div').removeClass('shake');
            $(element).closest('div').removeClass('has-danger');
        },
        submitHandler: function (form) {
            ajaxSend($(form));
            event.preventDefault();
        }
    });


    //Форма узнать цену
    $('#form_price').validate({
        rules: _NamePhoneEmail,
        messages: _MessagesForm,
        errorElement: "div",
        errorPlacement: function (error, element) {
            /*error.addClass("form-control-feedback");
             if (element.prop("type") === "checkbox") {
             error.insertAfter(element.parent("label"));
             } else {
             error.insertAfter(element);
             }*/
        },
        highlight: function (element, errorClass, validClass) {
            $(element).closest('div').addClass('shake');
            setTimeout(function () {
                $(element).closest('div').removeClass('shake');
            }, 1500);
            $(element).closest('div').addClass('has-danger');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest('div').removeClass('shake');
            $(element).closest('div').removeClass('has-danger');
        },
        submitHandler: function (form) {
            ajaxSend($(form));
            event.preventDefault();
        }
    });

    //Форма "Перезвоните мне"
    $("#api_form_recall").validate({
        rules: _NamePhone,
        messages: _MessagesForm,
        errorElement: "div",
        errorPlacement: function (error, element) {
            /*error.addClass("form-control-feedback");
             if (element.prop("type") === "checkbox") {
             error.insertAfter(element.parent("label"));
             } else {
             error.insertAfter(element);
             }*/
        },
        highlight: function (element, errorClass, validClass) {
            $(element).closest('div').addClass('shake');
            setTimeout(function () {
                $(element).closest('div').removeClass('shake');
            }, 1500);
            $(element).closest('div').addClass('has-danger');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).closest('div').removeClass('shake');
            $(element).closest('div').removeClass('has-danger');
        },
        submitHandler: function (form) {
            ajaxSend($(form));
            event.preventDefault();
        }
    });


});